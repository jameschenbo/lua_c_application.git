#include "lua/include/lua.h"
#include "lua/include/lualib.h"
#include "lua/include/lauxlib.h"

static int clua_add(lua_State* L, int a, int b)
{
	int sum = 0;

	/* 函数入栈 */
	lua_getglobal(L, "add");

	/* 第一个函数参数入栈 */
	lua_pushnumber(L, a);

	/* 第二个函数参数入栈 */
	lua_pushnumber(L, b);

	/* 执行函数调用。2表示有两个函数形参，1表示add函数只有一个返回值，调用lua_call函数后lua自动出栈参数和函数，并将函数的执行结果入栈 */

	/*
	 * 执行函数调用
	 * 2表示lua脚本中add函数需要输入两个函数参数
	 * 1表示lua脚本中add函数有一个返回值
	 * 执行完函数调用后，lua自动出栈函数和参数
	 */
	lua_call(L, 2, 1);

	/*
	 * 得到函数add函数执行结果
	 * -1表示最后一个返回值，因为lua的函数可以返回多个值的。
	 */
	sum = lua_tonumber(L, -1);

	/* 出栈一个数据。此时栈中存的是add函数的执行结果，所以需要出栈 */
	lua_pop(L, 1);

	return sum;
}
/**
 * 调用lua 函数,传递参数并获取返回值
 * lua_script/add.lua
 */
void example_add(void)
{
	int sum = 0;

	lua_State* L;

	L = luaL_newstate();  /* 创建一个句柄 */

	luaL_openlibs(L);     /* 打开lua库 */

#if 1
	if(luaL_dofile(L, "./lua_script/add.lua"))  /* 从lua脚本文件 中加载lua脚本语句 */
	{
		printf(" load lua script file error! \r\n");
		return;
	}
#else
	if(luaL_dostring(L, (const char *)"function add(a, b) return a + b end"))  /* 从字符串中加载lua脚本语句 */
	{
		printf(" LUA语句有误！\r\n");
		return -1;
	}
#endif

	sum = clua_add(L, 10, 20);
	printf(" sum = %d \r\n", sum);


	lua_close(L);  /* 关闭lua，清理内存 */
}

void load_config_file(lua_State* L, const char* fname, int *w, int *h)
{
	if(luaL_loadfile(L, fname) || lua_pcall(L, 0, 0,0)) {
		printf("load config file error\n");
	}

	//1.读变量配置

	//入栈操作,和出栈操作要对应
	lua_getglobal(L, "width");
	lua_getglobal(L, "height");

    //出栈操作,先压栈的,后出栈
	if(!lua_isnumber(L, -2)) {
		printf("width should be number!\n");
	}

	if(!lua_isnumber(L, -1)) {
		printf("height should be number!\n");
	}
	//转换数据类型
	*w = lua_tointeger(L, -2);
	*h = lua_tointeger(L, -1);

	//清空栈
	lua_settop(L, 0);

	//2.读 table 配置
	lua_getglobal(L, "sys_table_cfg");
	//入栈,指定位置
	lua_getfield(L, -1, "sex");
	lua_getfield(L, -2, "age");
	lua_getfield(L, -3, "port");
	lua_getfield(L, -4, "baud");
	lua_getfield(L, -5, "isSave");

    //出栈,和入栈顺序相反
    printf("sys_table_cfg:\n\n");
	printf("sex:%s\n",lua_tostring(L, -5));
	printf("age:%d\n",lua_tointeger(L, -4));
	printf("port:%s\n",lua_tostring(L, -3));
	printf("baud:%d\n",lua_tointeger(L, -2));
	printf("isSave:%d\n",lua_tointeger(L, -1));
	printf("\n");
	lua_settop(L, 0);
}
/**
 * 读取lua格式的配置文件
 * lua_script/config.lua
 * 配置文件有全局变量,有表
 */
void example_config_file(void)
{
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);

	int w, h;
	load_config_file(L, "./lua_script/config.lua", &w, &h);
	printf("width=%d,height=%d\n", w, h);
}
int main(int argc, char* argv[])
{
	// example_add();
	example_config_file();

	getchar();
	return 0;
}		