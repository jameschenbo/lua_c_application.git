# lua_c_application

#### 介绍
1.如何在windows 平台编译lua(lua动态库，luac.exe，lua.exe)
2.如何在C语言中执行lua脚本文件

#### 软件架构
- lua-5.4.6文件夹:lua源代码
- example文件夹:c语言调用lua脚本例程
- build.cmd:编译lua的脚本程序,执行该脚本会生成 lua文件夹,里面是动态库,lua.exe,luac.exe
- libgcc_s_dw2-1.dll:lua依赖的动态库

#### 安装教程

1.  修改GCC编译器路径，下面改为自己编译器路径，我使用的是QT5安装目录下的mingw编译器
2.  build.cmd 16行: set compiler_bin_dir=E:\Qt\Tools\mingw810_32\bin
3.  执行 build.cmd ，生成lua文件夹,如下:

    lua/bin/lua.exe       解析器
    lua/bin/luac.exe     编译器
    lua/bin/lua54.dll    动态库
    lua/doc  帮助文档
    lua/include 在其他平台使用编译器需要包含的头文件

4.编译结束后，将libgcc_s_dw2-1.dll 拷贝到 lua/bin目录
5.在 lua/bin 目录执行 lua.exe

#### 例程使用说明

1.  进入example文件夹
2.  c_call_main.c 是测试代码,在main函数中选择两个例程中的一个
3.  执行build.cmd编译,生成main.exe (build.cmd 中的GCC路径同样需要修改)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


